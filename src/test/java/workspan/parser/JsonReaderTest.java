package workspan.parser;

import org.junit.Test;
import workspan.graph.Entity;
import workspan.graph.Link;
import workspan.graph.Graph;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class JsonReaderTest {

  private static final JsonReader PARSER = new JsonReader();

  @Test
  public void shouldParseExample2() throws IOException {
    Path example2Path = Paths.get(getClass().getResource("example2.json").getPath());

    Graph graph = PARSER.parse(example2Path);

    Collection<Entity> entities = graph.getEntities();
    assertThat(entities, hasSize(7));

    Entity entity3 = Entity.of(3, "EntityA");
    Entity entity5 = Entity.of(5, "EntityB");
    Entity entity7 = Entity.of(7, "EntityC", "More details about entity C");
    Entity entity11 = Entity.of(11, "EntityD");
    Entity entity12 = Entity.of(12, "EntityB");
    Entity entity13 = Entity.of(13, "EntityC", "More details about entity C");
    Entity entity14 = Entity.of(14, "EntityD");

    Link v35 = Link.createAndConnect(entity3, entity5);
    Link v37 = Link.createAndConnect(entity3, entity7);
    Link v57 = Link.createAndConnect(entity5, entity7);
    Link v711 = Link.createAndConnect(entity7, entity11);
    Link v312 = Link.createAndConnect(entity3, entity12);
    Link v1313 = Link.createAndConnect(entity12, entity13);
    Link v1714 = Link.createAndConnect(entity13, entity14);

    assertThat(entities, containsInAnyOrder(entity3, entity5, entity7, entity11, entity12, entity13, entity14));
    assertThat(entity3.getLinks(), containsInAnyOrder(v35, v37, v312));
    assertThat(entity5.getLinks(), containsInAnyOrder(v35, v57));
    assertThat(entity7.getLinks(), containsInAnyOrder(v37, v57, v711));
    assertThat(entity11.getLinks(), containsInAnyOrder(v711));
    assertThat(entity12.getLinks(), containsInAnyOrder(v312, v1313));
    assertThat(entity13.getLinks(), containsInAnyOrder(v1313, v1714));
    assertThat(entity14.getLinks(), containsInAnyOrder(v1714));

  }
}

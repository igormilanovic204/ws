package workspan.graph;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import workspan.parser.JsonReader;
import workspan.parser.JsonReaderTest;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class GraphTest {

  private static final JsonReader PARSER = new JsonReader();

  @Rule
  public ExpectedException illegalArgumentExcepted = ExpectedException.none();

  @Test
  public void shouldFindEdgeWithAnId() {
    Graph graph = getGraph("example1.json");

    Entity Entity = graph.findEntityWithId(5);

    assertEquals(5L, Entity.getId());
    assertEquals("EntityB", Entity.getName());
    assertNull(Entity.getDescription());
  }

  @Test
  public void shouldThrowIllegalArgumentWhenEdgeWithGivenIdNotFound() {
    illegalArgumentExcepted.expect(IllegalArgumentException.class);
    illegalArgumentExcepted.expectMessage("Entity with an id : 123 not found.");

    Graph graph = getGraph("example1.json");

    graph.findEntityWithId(123);
  }

  @Test
  public void shouldTestClone() {
    Entity entity = Entity.of(0, "name-1", "description");
    Graph graph = new Graph(new HashMap<>());

    Entity cloneEntity = graph.cloneEntity(entity);

    assertEquals(1L, cloneEntity.getId());
    assertEquals("name-1", cloneEntity.getName());
    assertEquals("description", cloneEntity.getDescription());
  }

  private Graph getGraph(final String name) {
    Path path = Paths.get(JsonReaderTest.class.getResource(name).getPath());
    try {
      return PARSER.parse(path);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}

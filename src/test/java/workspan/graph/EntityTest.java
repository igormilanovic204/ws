package workspan.graph;

import org.junit.Test;

import java.util.Set;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class EntityTest {

  @Test
  public void shouldFindReachableEdgesWhenEdgesAreConnected() {
    Entity v1 = Entity.of(1, "name-1");
    Entity v2 = Entity.of(2, "name-2");
    Entity v3 = Entity.of(3, "name-3");
    Entity v4 = Entity.of(4, "name-4");

    connectEdges(v1, v2, v3, v4);

    Set<Entity> reachableVertices = v1.findReachableEntities();

    assertThat(reachableVertices, containsInAnyOrder(v2, v3, v4));
  }

  @Test
  public void shouldNotFindReachableEdgesWhenEdgesAreNotConnected() {
    Entity v1 = Entity.of(1, "name-1");
    Entity v2 = Entity.of(2, "name-2");
    Entity v3 = Entity.of(3, "name-3");
    Entity v4 = Entity.of(4, "name-4");

    Set<Entity> reachableVertices = v1.findReachableEntities();

    assertThat(reachableVertices, hasSize(0));
  }

  @Test
  public void shouldNotFindReachableEdgesWhenThereIsNotOutVertexFromStartEdge() {
    Entity v1 = Entity.of(1, "name-1");
    Entity v2 = Entity.of(2, "name-2");
    Entity v3 = Entity.of(3, "name-3");
    Entity v4 = Entity.of(4, "name-4");

    Link.createAndConnect(v2, v1);
    Link.createAndConnect(v3, v2);
    Link.createAndConnect(v4, v3);

    Set<Entity> reachableVertices = v1.findReachableEntities();

    assertThat(reachableVertices, hasSize(0));
  }

  @Test
  public void shouldNotFallIntoInfiniteLoopWhenEdgesAreConnectedCyclic() {
    Entity v1 = Entity.of(1, "name-1");
    Entity v2 = Entity.of(2, "name-2");

    Link.createAndConnect(v1, v2);
    Link.createAndConnect(v2, v1);

    Set<Entity> reachableVertices = v1.findReachableEntities();

    assertThat(reachableVertices, containsInAnyOrder(v2));
  }

  private void connectEdges(Entity entity1, Entity entity2, Entity entity3, Entity entity4) {
    Link.createAndConnect(entity1, entity2);
    Link.createAndConnect(entity2, entity3);
    Link.createAndConnect(entity3, entity4);
  }
}

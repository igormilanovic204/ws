package workspan.graph;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import workspan.Main;
import workspan.parser.JsonReader;
import workspan.parser.JsonReaderTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class IntegrationTest {

  private static final JsonReader PARSER = new JsonReader();

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final PrintStream originalOut = System.out;

  @Before
  public void setUpStream() {
    System.setOut(new PrintStream(outContent));
  }

  @After
  public void restoreStream() {
    System.setOut(originalOut);
  }

  @Test(timeout = 3_000)
  public void example1() throws JSONException, IOException {
    Path path = Paths.get(JsonReaderTest.class.getResource("example1.json").getPath());
    Graph graph = PARSER.parse(path);

    Main.task(5, graph, path);

    String expected = read(Paths.get(JsonReaderTest.class.getResource("example2.json").getPath()));

    JSONAssert.assertEquals(expected, outContent.toString(), false);
  }

  @Test(timeout = 3_000)
  public void example10k() throws IOException {
    Graph graph = PARSER.parse(Paths.get(JsonReaderTest.class.getResource("10kentities-100klinks.json").getPath()));
    Path path = Paths.get("example.json");

    Main.task(123, graph, path);
  }

  private String read(Path path) throws IOException {
      StringBuilder sb = new StringBuilder();
      Stream<String> lines = Files.lines(path);
      lines.forEach(sb::append);
      return sb.toString();
  }
}

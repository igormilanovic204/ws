package workspan.graph;

import java.util.Objects;

/**
 * Represents directed connection between two entities.
 */
public class Link {

  private Entity from;
  private Entity to;

  /**
   * Create new connection for two entities and connects it with them.
   *
   * @param from starting entity.
   * @param to finishing entity.
   * @return newly created link.
   */
  public static Link createAndConnect(Entity from, Entity to) {
    Link link = new Link();
    link.from = from;
    link.to = to;
    link.connect(from, to);
    return link;
  }

  private Link() { }

  private void connect(Entity from, Entity to) {
    from.addVertex(this);
    to.addVertex(this);
  }

  public Entity getFrom() {
    return from;
  }

  public Entity getTo() { return to; }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Link link = (Link) o;
    return from.equals(link.from) &&
            to.equals(link.to);
  }

  @Override
  public int hashCode() {
    return Objects.hash(from, to);
  }
}

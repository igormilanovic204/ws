package workspan.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Represents repository of all entities in graph.
 * {@link maxId} represents currently max value for Entity's id.
 */
public class Graph {

  private Map<Long, Entity> entities;
  private long maxId;

  public Graph(Map<Long, Entity> entities) {
    this.entities = entities;
    if (!entities.keySet().isEmpty()) {
      this.maxId = Collections.max(entities.keySet());
    }
  }

  /**
   * Tries to find entity with given id.
   * @param id id of required entity.
   * @return found entity.
   * @throws IllegalArgumentException if entity isn't found.
   */
  public Entity findEntityWithId(long id) {
    if (entities.containsKey(id)) {
      return entities.get(id);
    }

    throw new IllegalArgumentException("Entity with an id : " + id + " not found.");
  }

  /**
   * Clones given entity's name, description and increment id.
   * @param entity original entity.
   * @return cloned entity.
   */
  public Entity cloneEntity(final Entity entity) {
    Entity cloned = Entity.of(this.incrementAndGetId(), entity.getName(), entity.getDescription());
    entities.put(cloned.getId(), cloned);
    return cloned;
  }

  private long incrementAndGetId() {
    return ++maxId;
  }

  public Collection<Entity> getEntities() {
    return entities.values();
  }

  public Collection<Link> getLinks() {
    return getEntities()
            .stream()
            .flatMap(edge -> edge.getLinks().stream())
            .collect(Collectors.toSet());
  }
}

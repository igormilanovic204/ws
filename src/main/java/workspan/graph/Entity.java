package workspan.graph;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Represents one node in graph with all its links to other nodes.
 */
public class Entity {

  private long id;
  private String name;
  private Set<Link> links;
  private String description;

  public static Entity of(long id, String name) {
    Entity entity = new Entity();
    entity.id = id;
    entity.name = name;
    return entity;
  }

  public static Entity of(long id, String name, String description) {
    Entity entity = of(id, name);
    entity.description = description;
    return entity;
  }

  /**
   * Returns all adjacent entities. Entity is considered adjacent only if
   * given entity has a direct link to it.
   *
   * @return Set of adjacent entities.
   */
  private Set<Entity> findAdjacentEntities() {
    return this.links.stream().map(Link::getTo).collect(Collectors.toSet());
  }

  /**
   * Starting from this entity traverse graph and collect all reachable entities.
   *
   * @return Set of reachable entities.
   */
  public Set<Entity> findReachableEntities() {
    // TODO: Component base algorithm for traversing?
    Set<Entity> alreadyVisited = new HashSet<>();
    Set<Entity> reachableVertices = new HashSet<>();
    Stack<Entity> entitiesToProcess = new Stack<>();
    entitiesToProcess.push(this);
    while(!entitiesToProcess.isEmpty()) {
      Entity poppedEntity = entitiesToProcess.pop();
      if (!alreadyVisited.contains(poppedEntity)) {
        alreadyVisited.add(poppedEntity);
        for (Entity adjacentEntity : poppedEntity.findAdjacentEntities()) {
          entitiesToProcess.push(adjacentEntity);
          if (!adjacentEntity.equals(this))
            reachableVertices.add(adjacentEntity);
        }
      }
    }

    return reachableVertices;
  }

  void addVertex(final Link link) {
    this.links.add(link);
  }

  private Entity() {
    this.links = new HashSet<>();
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public Set<Link> getLinks() {
    return links;
  }

  public String getDescription() {
    return description;
  }

  /**
   * @return Set of entities which are pointing at this.
   */
  public Set<Entity> findEntitiesPointingAtThis() {
    return links.stream().filter(l -> l.getTo().equals(this)).map(Link::getFrom).collect(Collectors.toSet());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Entity entity = (Entity) o;
    return id == entity.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Entity{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
  }
}

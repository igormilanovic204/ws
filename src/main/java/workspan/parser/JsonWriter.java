package workspan.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import workspan.graph.Entity;
import workspan.graph.Link;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collection;

public class JsonWriter {

  private static final Gson GSON = new GsonBuilder()
          .setPrettyPrinting()
          .create();

  /**
   * Writes collection of entities and links to standard output.
   *
   * @param entities collection of entities.
   * @param links collection of links.
   */
  public void writeStdOut(final Collection<Entity> entities, final Collection<Link> links) {
    try (Writer writer = new PrintWriter(System.out)) {
      write(entities, links, writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void write(Collection<Entity> entities, Collection<Link> links, Writer writer) {
    JsonObject root = new JsonObject();
    root.add("entities", mapEntities(entities));
    root.add("links", mapLinks(links));
    GSON.toJson(root, writer);
  }

  private JsonArray mapEntities(Collection<Entity> entities) {
    JsonArray verticesArray = new JsonArray();
    for (Entity entity : entities) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.addProperty("entity_id", entity.getId());
      jsonObject.addProperty("name", entity.getName());
      jsonObject.addProperty("description", entity.getDescription());
      verticesArray.add(jsonObject);
    }
    return verticesArray;
  }

  private JsonArray mapLinks(Collection<Link> links) {
    JsonArray edgesArray = new JsonArray();
    for (Link link : links) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.addProperty("from", link.getFrom().getId());
      jsonObject.addProperty("to", link.getTo().getId());
      edgesArray.add(jsonObject);
    }
    return edgesArray;
  }
}

package workspan.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import workspan.graph.Entity;
import workspan.graph.Graph;
import workspan.graph.Link;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class JsonReader {

  /**
   * Reads content from given Path and converts it to representing Graph.
   *
   * @param path Path to file with json data.
   * @return representation of Graph.
   * @throws IOException in case if Path can't be found.
   */
  public Graph parse(final Path path) throws IOException {
    StringBuilder sb = new StringBuilder();
    Stream<String> lines = Files.lines(path);
    lines.forEach(sb::append);

    JsonObject root = (JsonObject) new JsonParser().parse(sb.toString());
    JsonArray entities = (JsonArray) root.get("entities");
    JsonArray links = (JsonArray) root.get("links");

    Map<Long, Entity> entitiesMap = parseEdges(entities);

    for (JsonElement linkElement : links) {
      JsonObject linkObject = linkElement.getAsJsonObject();
      long from = linkObject.get("from").getAsLong();
      long to = linkObject.get("to").getAsLong();
      Link.createAndConnect(entitiesMap.get(from), entitiesMap.get(to));
    }

    return new Graph(entitiesMap);
  }

  private Map<Long, Entity> parseEdges(JsonArray entities) {
    Map<Long, Entity> edges = new HashMap<>(entities.size());
    for (JsonElement entityElement : entities) {
      JsonObject entityObject = entityElement.getAsJsonObject();
      long id = entityObject.get("entity_id").getAsLong();
      String name = entityObject.get("name").getAsString();
      String description = null;
      JsonElement descriptionElement = entityObject.get("description");
      if (descriptionElement != null) {
        description = descriptionElement.getAsString();
      }
      edges.put(id, Entity.of(id, name, description));
    }
    return edges;
  }
}

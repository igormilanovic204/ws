package workspan;

import com.google.gson.JsonSyntaxException;
import workspan.graph.Entity;
import workspan.graph.Graph;
import workspan.graph.Link;
import workspan.parser.JsonReader;
import workspan.parser.JsonWriter;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Main {

  private static final JsonReader PARSER = new JsonReader();
  private static final JsonWriter WRITER = new JsonWriter();

  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("Input parameters must be in format: <inputfile> <entityid>");
      return;
    }

    String pathStr = args[0];
    String idStr = args[1];

    Path path;
    try {
      path = Paths.get(args[0]);
    } catch (InvalidPathException e) {
      System.err.println("Input param " + pathStr + " can not be converted to Path!\n" + e);
      return;
    }

    long entityId;
    try {
      entityId = Long.parseLong(idStr);
    } catch (NumberFormatException e) {
      System.err.println("Input parameter " + idStr + " couldn't be parse to long!\n" + e);
      return;
    }

    Graph graph;
    try {
      graph = PARSER.parse(path);
    } catch (IOException e) {
      System.err.println("Path " + path + " couldn't be parsed!");
      return;
    } catch (JsonSyntaxException e) {
      System.err.println("Malformed json file!\n" + e);
      return;
    }

    task(entityId, graph, path);
  }

  public static void task(long entityId, Graph graph, Path path) {
    /*
    1) Find the entity with the entityid given as a parameter on the command line. This is referred to as the
    initial entity below.
    */
    Entity startEntity = graph.findEntityWithId(entityId);

    /*
    2) Create a clone (copy) of the initial entity and assign it a new id. You can use any unique integer id that does
    not exist in the current list.
    */
    Entity clonedStartEntity = graph.cloneEntity(startEntity);
    Map<Entity, Entity> originalClonesMap = new HashMap<>();
    originalClonesMap.put(startEntity, clonedStartEntity);

    /*
    3) Clone all the related entities by traversing the links from the initial entity. This process should continue
    till all the related entities and links have been cloned. Note there may be loops in entities, i.e. an entity may
    link back to one createAndConnect its ancestors. This case needs to be handled
    */
    for (Entity reachableEntity : startEntity.findReachableEntities()) {
      Entity cloneEntity = graph.cloneEntity(reachableEntity);
      originalClonesMap.put(reachableEntity, cloneEntity);

      for (Entity entityPointingAtThis : reachableEntity.findEntitiesPointingAtThis()) {
        Entity cloned = originalClonesMap.get(entityPointingAtThis);
        if (cloned != null) {
          Link.createAndConnect(cloned, cloneEntity);
        }
      }
    }

    /*
    4) For the initial entity, any entities that link to it must now also link to the clone of the initial entity.
    */
    startEntity.findEntitiesPointingAtThis()
               .forEach(pointingEntity ->
                            Link.createAndConnect(pointingEntity, clonedStartEntity));

    /*
    5) All the new entities and links should be added back to the list of entities and links.
    */

    /*
    6) When the cloning is completed, the program should output the entities and links to standard output as valid JSON
    in the same format as the input file.
    */
    WRITER.writeStdOut(graph.getEntities(), graph.getLinks());
  }
}
